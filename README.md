A Python base image for development use. It *must* be used as a base image
to build the development image for a given service.

## On Build Triggers

Two flavours of this image are available for each Python version. The default one
has on build triggers that run _before_ your Dockerfile instructions. These triggers
will install the Python packages from a `requirements.txt` file.

A `requirements.txt` file must exist in the Python project directory.

Example Dockerfile using this, place the following in the Python project directory:
```
FROM octobotdev/python-backend-dev:latest
```

In case you need to do something _before_ installing your Python packages (such as
installing system dependencies) you can use the \*-no-onbuild variant for this and
run the install step yourself.

Example Dockerfile using this, place the following in the Python project directory:
```
FROM octobotdev/python-backend-dev:latest-no-onbuild
# Run something here...
COPY requirements.txt /usr/development/
RUN pip3 install -r /usr/development/requirements.txt
```